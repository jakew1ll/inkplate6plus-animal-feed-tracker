#if !defined(ARDUINO_INKPLATE6PLUS) && !defined(ARDUINO_INKPLATE6PLUSV2)
#error "Wrong board selection for this example, please select e-radionica Inkplate 6Plus or Soldered Inkplate 6Plus in the boards menu."
#endif

#include <Inkplate.h>
#include <ArduinoJson.h>
#include "SdFat.h"
#include <WiFi.h>       //Include ESP32 WiFi library to our sketch
// Fonts
#include "Fonts/FreeSansBold24pt7b.h"
#include "Fonts/FreeSans18pt7b.h"
#include "Fonts/FreeMonoOblique12pt7b.h"
// icons
#include "icons.h"

// declare functions
void drawAppTitle(String * title);
void mainDraw(String * title, struct animal* an, int animalCount, struct ui_touch_box* ta=NULL);
void drawAnimals(struct animal* an, int animalCount, struct ui_touch_box* ta=NULL, bool update=false);
void touchLoop(struct ui_touch_box* ta, struct animal* an, int animalCount);
void updateAnimal(struct animal* a);
int loadAnimalsFromSD(struct animal* ta);
void saveAnimalsToSD(struct animal* ta, int animalCount);
int loadWifiConfigFromSD(struct wifi_configuration* c);
void setTime();
void setupWifi();
void showBatteryStatus();

// globals and constants
struct animal {
  char name[32];
  char type[32];
  time_t last_fed_ts;
};

struct ui_touch_box {
  int x;
  int y;
  int height;
  int width;
};

struct wifi_configuration {
  char ssid[128];
  char password[128];
};

uint8_t touchRegs[8];                  // For setting touch on wake
Inkplate display(INKPLATE_1BIT);       // main inkplate display object

int touchLoops = 0;                    // Tracker for amount of loops to wait for
// Conversion factor for micro seconds to seconds
uint64_t const uS_TO_S_FACTOR = 1000000;
// bitmask for GPIO_34 which is connected to I/O expander INT pin
#define TOUCHPAD_WAKE_MASK (int64_t(1) << GPIO_NUM_34)

// ************* UI Constants **************** //
const GFXfont *animal_font = &FreeSans18pt7b;
const GFXfont *app_title_font = &FreeSansBold24pt7b;
const GFXfont *fed_font = &FreeMonoOblique12pt7b;
int app_title_cursor_x = 250;
int app_title_cursor_y = 70;
int box_height = 100;
int box_width = 500;
int box_spacing = 25;
int max_sub_text_width = 380;
int box_init_x = 35;
int box_init_y = 115;

// ************* Configurable items **************** //
String const app_title = "Animal Feed Tracker v1.1";
String const LOCAL_POSIX_TZ = "CST6CDT,M3.2.0,M11.1.0";  // Get value from: https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv
int const LOCAL_TZ_OFFSET = -6;
int const MAX_LOOPS = 60;                                // max # of loops to wait for touch, needs to be at least 60 normally (for 1 minute of dwell time before sleeping), 10 for testing
uint64_t const TIME_TO_SLEEP = 14400;                           // Time ESP32 will go to sleep (in seconds)
int const WIFI_TRY_COUNT = 10;
double const BAT_FULL_VOLTS = 4.2;
double const BAT_EMPTY_VOLTS = 3.42;
int const LAST_FED_REMINDER_SEC = 14400;            // The # of seconds for the last fed reminder (aka turning to black), 4hours
String const animal_config_path = "/AnimalFeedTracker/Animals.json";
String const app_config_path = "/AnimalFeedTracker/Config.json";

// ----- Start of logic ------

void setup() {

  Serial.begin(115200);
  display.begin();
  display.clearDisplay();

  // Init touchscreen and power it on after init (send false as argument to put it in deep sleep right after init)
  if (!display.tsInit(true)) {
    Serial.println("Touchscreen init fail");
    while (true);
  }

  // Display intro message
  display.setCursor(120, 300);
  display.setTextSize(5);
  display.print("Animal Feed Tracker v1.0"); // change this to the title function
  display.setCursor(350, 400);
  display.setTextSize(3);
  display.print("Loading...");
  display.partialUpdate();

  // init all the things WIFI, SD card, etc
  // Init SD card
  if (!display.sdCardInit()) {
    display.println("SD Card error!");
    display.partialUpdate();
    display.sdCardSleep();
    while (true);
  } else {
    display.print(".");
    display.partialUpdate();
  }

  // Init wifi
  setupWifi();

  // see if we need to init rtc
  display.rtcGetRtcData(); // Get RTC data
  // @TODO maybe store/retrieve preference for RTC setting like other apps
  // need to change this condition to check for network access
  setTime();

  display.print(".");
  display.partialUpdate();

  // Init key variables
  animal animals[5];
  ui_touch_box touchAreas[5];

  // Read animals from config
  int animalCount = loadAnimalsFromSD(animals);

  display.print(".");
  display.partialUpdate();
  display.clearDisplay();

  // Display main animal menu
  mainDraw(app_title, animals, animalCount, touchAreas);
  // @DEBUG_START animal box 0, this needs to move to a UI file
  // drawAnimals(animals, animalCount, touchAreas);
  // @DEBUG_END
  // push full display
  display.display();
  // loop for handling touch in area
  touchLoop(touchAreas, animals, animalCount);

  // @TODO FUTURE Webserver for setting config with QR code in right hand side for access on WIFI
  // @TODO FUTURE Send MQTT message for feeding stats before sleep for grafana to map

}

void loop() {
  // not using for this project

}

void touchLoop(struct ui_touch_box* ta, struct animal* an, int animalCount) {
  // when a area is touched get the RTC time and write it to the animal config
  // update animal display with new time. partial update?
  bool updated = 0;
  bool touched = 0;
  while (!touched & touchLoops < MAX_LOOPS) {
    touchLoops++;
    touched = display.tsAvailable();
    delay(500);
  }
  if (touched) {
    for (int i = 0; i < animalCount; ++i) {
      if (display.touchInArea(ta[i].x, ta[i].y, ta[i].width, ta[i].height)) {
        updated = 1;
        // Update animal ts
        updateAnimal(&an[i]);
        saveAnimalsToSD(an, animalCount);
        drawAnimals(an, animalCount, NULL, updated);
        display.display(); 
      }
    }
  } else {
    // prep for sleep
    display.sdCardSleep();
    // disconnect wifi
    if (WiFi.status() == WL_CONNECTED) {
      WiFi.disconnect(true, false);
    }
    display.frontlight(false); // Disable frontlight (to save power)

    // sleep w/ wake on touch
    // Setup I/O expander interrupts
#ifdef ARDUINO_INKPLATE6PLUS
    display.setIntOutput(1, false, false, HIGH);
    display.setIntPin(PAD1, RISING, IO_INT_ADDR);
    display.setIntPin(PAD2, RISING, IO_INT_ADDR);
    display.setIntPin(PAD3, RISING, IO_INT_ADDR);
#elif defined(ARDUINO_INKPLATE6PLUSV2)
    display.setIntPin(PAD1, IO_INT_ADDR);
    display.setIntPin(PAD2, IO_INT_ADDR);
    display.setIntPin(PAD3, IO_INT_ADDR);
#endif
    // Go to sleep for TIME_TO_SLEEP seconds
    esp_sleep_enable_timer_wakeup((TIME_TO_SLEEP * uS_TO_S_FACTOR));
    // Enable wakeup from deep sleep on gpio 36 (wake button)
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_36, LOW);
    // Enable wake from I/O expander port on gpio 34 (touch)
#ifdef ARDUINO_INKPLATE6PLUS
    esp_sleep_enable_ext1_wakeup(TOUCHPAD_WAKE_MASK, ESP_EXT1_WAKEUP_ANY_HIGH);
#elif defined(ARDUINO_INKPLATE6PLUSV2)
    esp_sleep_enable_ext1_wakeup(TOUCHPAD_WAKE_MASK, ESP_EXT1_WAKEUP_ALL_LOW);
#endif
    // Go to sleep
    esp_deep_sleep_start();
  }
  // keep going
  updated = 0;
  touchLoop(ta, an, animalCount);
}

void updateAnimal(struct animal* a) {
  // Get fresh RTC data
  display.rtcGetRtcData(); 
  // set fed time
  a->last_fed_ts = display.rtcGetEpoch();
}

// Returns count of animals, assign animal data to param passed in
int loadAnimalsFromSD(struct animal* ta){
  int count = 0;
  if ((sd).begin(15, SD_SCK_MHZ(10))) {
    File file;
    JsonDocument doc;
    if (file.open(animal_config_path.c_str(), FILE_READ)) {
      if (file.available()) {
        DeserializationError error = deserializeJson(doc, file);
        if (error) {
          Serial.println(F("Failed to read file, using default configuration"));
          Serial.println(error.f_str());
        }
        JsonArray dataArray = doc["animals"];
        for(JsonVariant v : dataArray) {
          strlcpy(ta[count].name, v["name"], 32);
          strlcpy(ta[count].type, v["type"], 32);
          ta[count].last_fed_ts = v["last_fed_ts"];
          count++;
        }
      }
    }
    file.close();
  }
  return count;
}

void saveAnimalsToSD(struct animal* ta, int animalCount) {
  if (sd.begin(15, SD_SCK_MHZ(10))) {
    if (sd.exists(animal_config_path.c_str())) {
      sd.remove(animal_config_path.c_str());
    }
    File file;
    JsonDocument doc;
    if (file.open(animal_config_path.c_str(), FILE_WRITE)) {
      // Setup json doc
      JsonArray dataArray = doc["animals"].to<JsonArray>();
      for (int i = 0; i < animalCount; ++i) {
        // setup animal object
        JsonObject object = dataArray.add<JsonObject>();
        // add the animal to the array
        object["name"] = ta[i].name;
        object["type"] = ta[i].type;
        object["last_fed_ts"] = ta[i].last_fed_ts;
      }
      if (serializeJson(doc, file) == 0) {
        Serial.println(F("Failed to write to file"));
      }
    }
    file.close();
  }
}

int loadWifiConfigFromSD(struct wifi_configuration* c){
  int count = 0;
  if ((sd).begin(15, SD_SCK_MHZ(10))) {
    File file;
    JsonDocument doc;
    if (file.open(app_config_path.c_str(), FILE_READ)) {
      if (file.available()) {
        DeserializationError error = deserializeJson(doc, file);
        if (error) {
          Serial.println(F("Failed to read config file, using default configuration"));
          Serial.println(error.f_str());
        } else {
          JsonObject data = doc["wifi_credentials"].as<JsonObject>();
          for(JsonPair kv : data) {
            strlcpy(c[count].ssid, kv.key().c_str(), 128);
            strlcpy(c[count].password, kv.value().as<const char*>(), 128);
            count++;
          }
        }
      }
    }
    file.close();
  }
  return count;
}

// parts of this needs to move to generatedUI.h eventually
void drawAnimals(struct animal* an, int animalCount, struct ui_touch_box* ta, bool update) {
  int box_start_x = box_init_x;
  int box_start_y = box_init_y;

  for (int i = 0; i < animalCount; ++i) {    
    int16_t temp_x, temp_y;
    uint16_t temp_w, temp_h;
    String last_fed = "";
    struct tm fed_date_time, now_date_time;
    char temp[20];
    // Get fresh RTC data
    display.rtcGetRtcData(); 
    // calculate last fed
    time_t nowEpoch = display.rtcGetEpoch();
    // calculate last fed
    localtime_r(&an[i].last_fed_ts, &fed_date_time);
    localtime_r(&nowEpoch, &now_date_time);
    // is it "today", "yeasterday" , other other Date
    if (fed_date_time.tm_mon == now_date_time.tm_mon & fed_date_time.tm_mday == now_date_time.tm_mday) {
      // today 
      sprintf(temp, "Today @ %.2d%c%.2d", 
        fed_date_time.tm_hour, 
        ':', 
        fed_date_time.tm_min);
    } else if (fed_date_time.tm_mon == now_date_time.tm_mon & (fed_date_time.tm_mday + 1) == now_date_time.tm_mday) {
      // yesterday
      sprintf(temp, "Yesterday @ %.2d%c%.2d", 
        fed_date_time.tm_hour, 
        ':', 
        fed_date_time.tm_min);
    } else {
      // Just display date and hour (if available)
      sprintf(temp, "%.2d%c%.2d @ %.2d%c%.2d", 
        (fed_date_time.tm_mon + 1), 
        '/', 
        fed_date_time.tm_mday,
        fed_date_time.tm_hour, 
        ':', 
        fed_date_time.tm_min);
    }
    last_fed += temp;

    display.setFont(animal_font); 
    display.setTextSize(1);  
    // figure out what out text area is
    display.getTextBounds(last_fed.c_str(), (box_start_x+115), (box_start_y+84), &temp_x, &temp_y, &temp_w, &temp_h);
    // get max width
    temp_w = max(temp_w, (uint16_t) (box_width - 115));

    // draw bound box, 2 boxs for shadow effect
    display.drawRoundRect(box_start_x, box_start_y, box_width, box_height, 10, BLACK);
    // shadow box
    display.drawRoundRect(box_start_x-2, box_start_y+1, box_width+4, box_height+3, 10, BLACK);
    // Animal name text
    display.setFont(animal_font);
    display.setTextColor(BLACK, WHITE);    
    display.setTextSize(1);    
    display.setCursor(box_start_x+110, box_start_y+40);
    display.print(an[i].name);
    // print last fed  
    display.setCursor((box_start_x+115), (box_start_y+84));
    display.setFont(animal_font);  
    display.setTextSize(1);    
    // if it is > 4 hrs from last fed make it white text with black background
    if ((nowEpoch - an[i].last_fed_ts) > LAST_FED_REMINDER_SEC) {
      display.setTextColor(WHITE, BLACK);
      display.fillRoundRect(((uint16_t)(box_start_x+115) - (uint16_t)5), ((uint16_t)(box_start_y+84) - temp_h + (uint16_t)5), temp_w, (temp_h + (uint16_t)10), 5, BLACK);
      // Serial.printf("%i , %i,  %i,  %i \n", ((uint16_t)(box_start_x+115) - (uint16_t)4), ((uint16_t)(box_start_y+84) - temp_h + (uint16_t)5), temp_w, (temp_h + (uint16_t)10));
    } else {
      display.setTextColor(BLACK, WHITE);    
      // if this is an update clear the words
      if (update) {
        display.fillRoundRect(((uint16_t)(box_start_x+115) - (uint16_t)5), ((uint16_t)(box_start_y+84) - temp_h + (uint16_t)5), temp_w, (temp_h + (uint16_t)10), 5, WHITE);
      }
    }
    display.print(last_fed.c_str()); 
    // display icon
    if (strcmp(an[i].type , "Cat") == 0 ) {
      // Serial.println("Drawing cat icon");
        display.drawBitmap(box_start_x+5, box_start_y+2, icon_cat_icon, 96, 96, BLACK, WHITE);
    } else if (strcmp(an[i].type , "Dog") == 0 ) {
        display.drawBitmap(box_start_x+5, box_start_y+2, icon_dog_icon, 96, 96, BLACK, WHITE);
    } else if (strcmp(an[i].type , "Chicken" ) == 0 ) {
      // Serial.println("Drawing chicken icon");
        display.drawBitmap(box_start_x+5, box_start_y+2, icon_chicken_icon, 96, 96, BLACK, WHITE);
    } else if (strcmp(an[i].type , "Pig") == 0 ) {
        display.drawBitmap(box_start_x+5, box_start_y+2, icon_pig_icon, 96, 96, BLACK, WHITE);
    }
    
    // set touch areas to loop on later
    if (ta != NULL) {
      ta[i].x = box_start_x;
      ta[i].y = box_start_y;
      ta[i].height = box_height;
      ta[i].width = box_width;
    }
    // increment spacing/starts
    box_start_y += (box_height + box_spacing);
  }
}

void mainDraw(const String title, struct animal* an, int animalCount, struct ui_touch_box* ta) {
  drawAppTitle(title);
  drawAnimals(an, animalCount, ta);
  showBatteryStatus();
}

void drawAppTitle(String title) {
  display.setFont(app_title_font);
  display.setTextColor(BLACK, WHITE);    display.setTextSize(1);    display.setCursor(app_title_cursor_x, app_title_cursor_y);
  display.print(title);
}

void showBatteryStatus() {
  int16_t width, height;
  int16_t temp_x, temp_y;
  uint16_t temp_w, temp_h;
  // Get battery voltage
  double voltage = display.readBattery();    
  // @TODO Figure out the bottom right corner then come out from there

  width = display.width();
  height = display.height();

  display.getTextBounds("100%", 100, 100, &temp_x, &temp_y, &temp_w, &temp_h);
  display.drawImage(battSymbol, (width - (int16_t) 110), (height - (int16_t) 50), 106, 45, BLACK); // Draw battery symbol at position X=100 Y=100
  display.setCursor((width - temp_w - (int16_t) 115), (height - (temp_h/2)));
  display.setTextColor(BLACK, WHITE); 
  display.printf("%i%%", (int) (((voltage - BAT_EMPTY_VOLTS) / (BAT_FULL_VOLTS - BAT_EMPTY_VOLTS)) * 100)); // Print battery voltage
  Serial.printf("%i%% \n", (int) (((voltage - BAT_EMPTY_VOLTS) / (BAT_FULL_VOLTS - BAT_EMPTY_VOLTS)) * 100)); // @DEBUG
  Serial.printf("%fv \n", (voltage)); // @DEBUG
}

void setTime() {
  struct tm t;
  // set TZ
  setenv("TZ",LOCAL_POSIX_TZ.c_str(),1);
  tzset();
  // use ntp client get the time and set it
  configTime(LOCAL_TZ_OFFSET * 3600, 3600, "pool.ntp.org", "time.nist.gov"); 

  time_t nowSecs = time(nullptr);
  while (nowSecs < 8 * 3600 * 2)
  {
      delay(500);
      // Serial.print(F(".")); // @DEBUG
      display.print(".");
      display.partialUpdate();
      yield();
      nowSecs = time(nullptr);
  }
  // Set system time
  localtime_r(&nowSecs, &t);
  // Set RTC time a bit different
  display.rtcSetTime(t.tm_hour, t.tm_min, t.tm_sec);
  display.rtcSetDate(t.tm_wday, (t.tm_mday + 1), (t.tm_mon + 1), t.tm_year + 1900);
}

void setupWifi () {

  WiFi.mode(WIFI_STA);
  // Get configurations
  wifi_configuration configs[4];
  int configCount = 0;
  configCount = loadWifiConfigFromSD(configs);
  // Status update
  display.print(".");
  display.partialUpdate();

  if (configCount < 1) {
    Serial.println("No WIFI config, restarting"); 
    delay(100);
    ESP.restart();
  }
  
  for (int i = 0; i < configCount-1; i++) {
    int cnt = 0;
    WiFi.begin(configs[i].ssid, configs[i].password);
    while ((WiFi.status() != WL_CONNECTED) & cnt < WIFI_TRY_COUNT) {
        // Status update
        display.print(".");
        display.partialUpdate();
        delay(500);
        yield();
        ++cnt;
    }
    if (WiFi.status() == WL_CONNECTED) {
      i = configCount;
    }
  }
  // double check we got a connection 
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Can't connect to WIFI, restarting");
    delay(100);
    ESP.restart();
  }
  Serial.println(WiFi.localIP()); // @DEBUG
}
