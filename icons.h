/*
icons.h
Inkplate 6 Arduino library
Jacob Williams 
May 18, 2024

This code is released under the MIT License 
Please review the LICENSE file included with this example.
Distributed as-is; no warranty is given.
*/

#ifndef ICONS_H
#define ICONS_H

#include "binary_Icons/battSymbol.h"
// icon attribution : <a href="https://www.flaticon.com/free-icons/pig" title="pig icons">Icons created by photo3idea_studio - Flaticon</a>
#include "binary_Icons/icon_pig_icon.h"
#include "binary_Icons/icon_chicken_icon.h"
#include "binary_Icons/icon_dog_icon.h" 
#include "binary_Icons/icon_cat_icon.h"

#endif