# Animal Feed Tracker

![Device Screenshot](./thumbnail.jpg)

Made for the [inkplate6plus](https://www.crowdsupply.com/soldered/inkplate-6plus) that uses ESP32 and a ePaper display with touch capabilities. This project was to enable tracking when an animal(s) were fed last. It has been a while since I did any lower level programming. So, the code is shit. I may refactor it, when I get time, into beautiful C++ objects and use anti-patterns to get some reusable components out of it for other projects.

## Features

- Loads time from NTP server
    - Why it requires WIFI access
- Stores 2 configuration files on the SD card
    - 1 for animals and last fed data
    - 1 for wifi credentials
- WIFI credentials configuration supports multiple networks
- You can wake the device by the wake button on the left side or by touching the screen
- Stays awake for ~1.5 minutes for touching/updating animals fed
- Wakes up at least once per hour

### Usage

Touch once to wake up. Touch an animal box/button (anywhere within the box) to update the animal last fed date-time stamp. Device stays awake for around 1.5 minutes before going to sleep to conserve battery power. Device wakes up once every hour to update the display for the last fed date and time. 

The last fed date and time translate "Today" if it is today's date, "Yesterday" for the day before today's date, or the month and day if anything else. Time is always shown in 24hr format (or military time). 

If the last fed date-time stamp was less than 4 hours it is displayed with white background and black text. If the last fed date-time stamp is/was greater than 4 hours it will be shown with white text in a black background to pull the user's attention to it. Most animals need to be fed several times a day in small portions. 

## Future features

- Emit MQTT messages so that feedings can be tracked over time in influxdb and grafana
- Display QR code to access a web UI for configuration on the device
- backlighting for night time usage
- Displaying current time/clock

## Customizing for you

### Configuration

#### Timezone

Change the following constants and build and push/deploy to change for your timezone. `LOCAL_POSIX_TZ` is the value from [this library table](https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv) . `LOCAL_TZ_OFFSET` is the signed integer for your timezone relative from gmt. Ex. CST is -6 hours so the value would be `-6`.

### Configuration Files

See the [example configs](configs/) for sample config files. 

#### Animals.json Structure

Max 5 animals supported (for now).

```json
{
    "animals": [
        {
            "name": "Penelope",
            "type": "Pig",
            "last_fed_ts": 1714750097
        }
        ...
    ]
}
```


#### Config.json Structure

Max 4 networks supported (for now).

```json
{
  "wifi_credentials": {
    "SSID": "PASSPHRASE",
    ...
  }
}
```

### Adding icons/types of animals

@TODO


# Icon Attribution

Icons provided are from [Icons created by photo3idea_studio - Flaticon](https://www.flaticon.com/free-icons/pig)